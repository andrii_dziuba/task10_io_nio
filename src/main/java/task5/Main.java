package task5;

import java.io.File;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        new Main().programLoop();
    }

    //TODO: choosing initial path as args param
    File currentPath = new File("C:\\Users\\Андрiй\\IdeaProjects");

    /**
     * Usage <code>cd directory_name</code> to go over specified directory. <code>cd .</code> - to current directory.
     * <code>cd ..</code> - to parent directory.
     */
    public void programLoop() {
        printCurrentFolder();
        String[] command = new String[2];
        try (Scanner scanner = new Scanner(System.in)) {
            while (scanner.hasNext()) {
                String scanned = scanner.nextLine();
                command = scanned.split("[ ]");

                if (command[0].toLowerCase().equals("cd")) {
                    if (command[1].equals(".")) {
                        printCurrentFolder();
                        continue;
                    } else if (command[1].equals("..")) {
                        currentPath = currentPath.getParentFile();
                        printCurrentFolder();
                    } else {
                        boolean found = false;
                        for (File file : currentPath.listFiles()) {
                            if (file.getName().toLowerCase().equals(command[1].toLowerCase()) && file.isDirectory()) {
                                currentPath = file.getAbsoluteFile();
                                printCurrentFolder();
                                found = true;
                                break;
                            }
                        }
                        if(!found) {
                            System.out.println("Directory not found");
                        }
                    }
                } else if (command[0].toLowerCase().equals("q")) {
                    break;
                } else {
                    System.out.println("Bad command");
                }
            }
        }
    }

    /**
     * Loops over available files and prints information about it.
     */
    void printCurrentFolder() {
        File[] files = currentPath.listFiles();
        for (File file : files) {
            System.out.println((file.isDirectory() ? "Directory: " : "File: ") + file.getName() +
                    " [" + (file.canRead() ? "readble" : "not readable") + ", "
                    + (file.isHidden() ? "hidden" : "not hidden") + "]");
        }
    }

}
