package task1;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public class Ship implements Serializable {

    public static transient String metaData = "This is class for saving and operating information about droid ships. DESTROY JEDI'S";

    private List<Droids> droidsOnBoard = null;
    private String shipName = null;


}
