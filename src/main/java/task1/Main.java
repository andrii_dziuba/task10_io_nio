package task1;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Droids> droidList = Arrays.asList(
                new Droids(Droids.DroidType.BATTLE_DROID, 1000),
                new Droids(Droids.DroidType.HEAVY_BATTLE_DROID, 600),
                new Droids(Droids.DroidType.DROIDEKA, 200)
        );

        Ship imperianCutter = new Ship(droidList, "Cutter");

        System.out.println("After create:");
        System.out.println(imperianCutter);

        ObjectWriterReader objectWriterReader = new ObjectWriterReader(new File("ships.dat"));
        objectWriterReader.writeObject(imperianCutter);

        System.out.println("After deserialization:");
        System.out.println(objectWriterReader.readObject());

    }
}
