package task1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Droids implements Serializable {

    private DroidType droidType;
    private int count;

    @Getter
    enum DroidType {
        BATTLE_DROID(10, 100), DROIDEKA(15, 500), HEAVY_BATTLE_DROID(50, 400);

        private int damage;
        private int healthPoints;

        DroidType(int damage, int healthPoints) {
            this.damage = damage;
            this.healthPoints = healthPoints;
        }
    }
}
