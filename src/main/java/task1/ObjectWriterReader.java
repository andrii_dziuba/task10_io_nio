package task1;

import java.io.*;
import java.util.Objects;

public class ObjectWriterReader {

    private final File file;

    public ObjectWriterReader(File file) {
        Objects.requireNonNull(file, "File variable must be not null");

        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.file = file;
    }

    public void writeObject(Object obj) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))){
            oos.writeObject(obj);
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object readObject() {
        Object read = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            read = ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return read;
    }
}
