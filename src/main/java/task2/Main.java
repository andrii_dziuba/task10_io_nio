package task2;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    static String format = "%s: %d ms";

    static int size = (1 << 20) * 50;

    public static void main(String[] args) {
        long begin = 0, end = 0;

//        File withoutBuffer = new File("writewithoutbuffer.txt");
//        try (OutputStream outputStream = new FileOutputStream(withoutBuffer)) {
//            begin = System.currentTimeMillis();
//            writeToStream(size, outputStream);
//            end = System.currentTimeMillis();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println(String.format(format, "Writing without buffer", end - begin));
//
//        try (InputStream inputStream = new FileInputStream(withoutBuffer)) {
//            begin = System.currentTimeMillis();
//            readFromStream(inputStream);
//            end = System.currentTimeMillis();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println(String.format(format, "Reading without buffer", end - begin));

        int bufferSize = size<<1;
        File withBuffer = new File("writewithbuffer.txt");
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(withBuffer), bufferSize)) {
            begin = System.currentTimeMillis();
            writeToStream(size, outputStream);
            end = System.currentTimeMillis();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(String.format(format, "Writing with buffer (buffer size: " + bufferSize + ")", end - begin));

        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(withBuffer), bufferSize)) {
            begin = System.currentTimeMillis();
            readFromStream(inputStream);
            end = System.currentTimeMillis();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(String.format(format, "Reading with buffer (buffer size: " + bufferSize + ")", end - begin));
    }

    public static void writeToStream(int size, OutputStream os) throws IOException {
        for(int i = 0; i<size; i++) {
            os.write(1);
        }
        os.flush();
    }

    static int[] readFromStream(InputStream is) throws IOException {
        int[] ints = new int[size];
        int counter = 0;
        int read;
        while ((read = is.read()) != -1) {
            ints[counter++] = read;
        }
        return ints;
    }

}
