package task4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


@lombok.RequiredArgsConstructor
public class CommentSearcher {

    private final File file;

    /**
     * args[0] must contain file name with path to handle.
     * @param args
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Please, specify the file name");
            return;
        }

        File file = new File(args[0]);
        if (!file.exists()) {
            System.out.println("File does not exist");
            return;
        }

        new CommentSearcher(file).printComments();
    }

    /**
     * Seeks and prints whole the comments of specified file in <code>args</code> parameter.
     */
    private void printComments() {
        int read = 0;
        int prevChar = 0;

        boolean printing = false;
        boolean lineComment = false;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.file))) {
            while ((read = bufferedReader.read()) != -1) {
                if (read == '/' && prevChar == '/' && !printing) {
                    System.out.print("NEW COMMENT:\n/");
                    printing = true;
                    lineComment = true;
                }
                if (read == '*' && prevChar == '/' && !printing) {
                    System.out.print("NEW COMMENT:\n/");
                    printing = true;
                }
                if (read == '/' && prevChar == '*') {
                    System.out.println("/");
                    printing = false;
                }

                if(printing) {
                    System.out.printf("%c", read);
                }
                if(lineComment && read == 10) {
                    printing = lineComment = false;
                }
                prevChar = read;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // THIS IS COMMMMMMMMMENT!!!

    // And this
    /*
    I can change the world. Perhaps.
     */
}
