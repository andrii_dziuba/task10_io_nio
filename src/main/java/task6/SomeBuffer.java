package task6;

import lombok.Getter;

import java.io.IOException;
import java.nio.*;
import java.nio.channels.FileChannel;

public class SomeBuffer {
    private FileChannel channel = null;

//    Try to create SomeBuffer class, which can be used for read and write data
//    from/to channel (Java NIO)
//
//    Write client-server program using NIO (+ if you want, other implementation
//using IO). E.g. you have one server and multiple clients. A client can send
//    direct messages to other client.
    private ByteBuffer byteBuffer = null;

    public SomeBuffer(FileChannel channel) {
        this.channel = channel;
    }

    public byte[] read() throws IOException {
        if(byteBuffer == null || byteBuffer.capacity() < channel.size()) {
            byteBuffer = ByteBuffer.allocate(Math.toIntExact(channel.size()));
        }
        channel.read(this.byteBuffer);
        return byteBuffer.array();
    }

    public void write(byte[] data) throws IOException {
        if(byteBuffer == null || byteBuffer.capacity() < data.length) {
            byteBuffer = ByteBuffer.allocate(data.length);
        }
        byteBuffer.put(data);
        System.out.println(new String(byteBuffer.array()));

        int write = channel.write(this.byteBuffer);

        System.out.println(write);
    }

}
