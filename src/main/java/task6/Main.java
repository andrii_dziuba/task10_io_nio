package task6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Main {

    public static void main(String[] args) {
        new Main();
    }

    Main() {
        RandomAccessFile file = null;
        try {
            file = new RandomAccessFile("task6.txt", "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FileChannel fileChannel = file.getChannel();
        SomeBuffer someBuffer = new SomeBuffer(fileChannel);

        try {
            someBuffer.write("123sdad".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fileChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
