package task3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {



    public static void main(String[] args) {
        File file = new File("task3.txt");
        try {
            MyImplementationOfInputStreamWithPushback pushback =
                    new MyImplementationOfInputStreamWithPushback(
                            new FileInputStream(file), 5);

            System.out.println(pushback.read());
            System.out.println(pushback.read());
            System.out.println(pushback.read());
            System.out.println(pushback.read());

            pushback.pushback(0);
            pushback.pushback(0);
            pushback.pushback(0);

            int read = 0;
            while((read = pushback.read()) != -1) {
                System.out.println(read);
            }

            System.out.println("Zeroes was pushed back to the stream and read from");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
