package task3;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

public class MyImplementationOfInputStreamWithPushback extends InputStream {

    private final InputStream inputStream;
    private int initialCapacity;
    private byte[] buffer = null;

    private int position;

    public MyImplementationOfInputStreamWithPushback(InputStream in, int capacity) {
        this.initialCapacity = capacity;
        this.inputStream = in;
        buffer = new byte[initialCapacity];
        position = capacity;
    }

    @Override
    public int read() throws IOException {
        if(inputStream == null) {
            throw new IOException("Stream is closed");
        }
        if (position < buffer.length) {
            return buffer[position++] & 0xff;
        }
        return inputStream.read();
    }

    public void pushback(int i) throws IOException {
        if(inputStream == null) {
            throw new IOException("Stream is closed");
        }
        if (position == 0) {
            throw new IOException("Push back buffer is full");
        }
        buffer[--position] = (byte)i;
    }
}
