package task7.client;

import task7.messages.Message;

import java.io.IOException;
import java.util.Scanner;

public class GUI {

    private static GUI singleton = null;

    private GUI() {

    }

    public static GUI getInstance() {
        if(singleton == null) {
            return singleton = new GUI();
        }
        return singleton;
    }

    public static void main(String[] args) {
        GUI.getInstance().print("Connecting...");
        Client client = new Client(31444);
        client.startClient();

        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            try {
                client.sendMessage(new Message(scanner.nextLine()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void print(String s) {
        System.out.println(s);
    }

    public void print(Message message) {
        System.out.println(message.getData());
    }
}
