package task7.client;

import task7.messages.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client implements Runnable {

    private transient ObjectInputStream inputStream = null;
    private transient ObjectOutputStream outputStream = null;
    private transient Socket socket = null;

    private Thread clientThread = null;

    public Client(int port) {
        try {
            socket = new Socket("localhost", port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        clientThread = new Thread(this);
    }

    private void setupStreams() {
        try {
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.flush();
            inputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            System.err.println("Streams setup exception");
        }
    }

    public void startClient() {
        setupStreams();
        clientThread.start();
    }

    public void sendMessage(Message message) throws IOException {
        outputStream.writeObject(message);
    }

    public void run() {
        System.out.println("client running");
        while(true) {
            try {
                Message message = (Message) inputStream.readObject();

                GUI.getInstance().print(message);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
