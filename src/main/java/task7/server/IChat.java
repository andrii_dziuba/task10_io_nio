package task7.server;

import task7.messages.Message;

public interface IChat {

    void onClientWrote(ClientHandler client, Message message);
}
