package task7.server;

import lombok.RequiredArgsConstructor;
import task7.client.Client;
import task7.messages.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.channels.SocketChannel;

public class ClientHandler implements Runnable{

    private Socket socket = null;
    private int clientID;
    private Thread clientThread = null;

    private ObjectInputStream inputStream = null;
    private ObjectOutputStream outputStream = null;

    public ClientHandler(Socket socket, int id) {
        this.socket = socket;
        try {
            this.inputStream = new ObjectInputStream(socket.getInputStream());
            this.outputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.clientThread = new Thread(this);
        this.clientID = id;
    }

    public void startHandling() {
        clientThread.start();
    }

    @Override
    public void run() {
        while(true) {
            try {
                Message message = (Message) inputStream.readObject();
                System.out.println("Message from client with id: " + clientID + ". " + message);
                Server.getInstance().onClientWrote(this, message);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(Object message) throws IOException {
        outputStream.writeObject(message);
    }

    public int getClientID() {
        return clientID;
    }
}
