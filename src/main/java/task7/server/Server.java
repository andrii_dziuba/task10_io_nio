package task7.server;

import task7.messages.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server implements Runnable, IChat {

    private final int port;
    private ServerSocket serverSocket = null;

    private transient Thread serverThread = null;
    private transient List<ClientHandler> clients;

    private static Server singleton = null;

    private Server(int port) {
        this.port = port;
        serverThread = new Thread(this);
        clients = new ArrayList<>();
    }

    public static void main(String[] args) {
        Server.createServer(31444);
        try {
            Server.getInstance().startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createServer(int port) {
        singleton = new Server(port);
    }

    public static Server getInstance() {
        return singleton;
    }

    /**
     * Starts the server on specified <code>port</code>.
     * @throws IOException if <code>port</code> is busy.
     */
    public void startServer() throws IOException {
        serverSocket = new ServerSocket(port);
        serverThread.start();
    }

    @Override
    public void run() {
        while(true) {
            Socket accept = null;
            try {
                 accept = serverSocket.accept();
            } catch (IOException e) {
                System.err.println("Exception: " + e);
            }
            System.out.println("Someone connected.");
            ClientHandler clientHandler = new ClientHandler(accept, clients.size());
            clientHandler.startHandling();
            clients.add(clientHandler);


        }
    }

    /**
     * Callback method, which are called when someone sent message to server.
     * @param client client
     * @param message message
     */
    @Override
    public void onClientWrote(ClientHandler client, Message message) {
        clients.forEach(i -> {
            if(i.getClientID() != client.getClientID()) {
                try {
                    i.sendMessage(new Message("Client " + client.getClientID() + ": " + message.getData()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
