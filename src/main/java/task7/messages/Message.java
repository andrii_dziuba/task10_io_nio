package task7.messages;

import lombok.Data;

import java.io.Serializable;

@Data
public class Message implements Serializable {

    private Object data;

    public Message(String s) {
        data = s;
    }
}
