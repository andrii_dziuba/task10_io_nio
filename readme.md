***Task 1*** Object serialization  
  
After create:  
Ship(droidsOnBoard=[Droids(droidType=BATTLE_DROID, count=1000), Droids(droidType=HEAVY_BATTLE_DROID, count=600), Droids(droidType=DROIDEKA, count=200)], shipName=Cutter)  
After deserialization:  
Ship(droidsOnBoard=[Droids(droidType=BATTLE_DROID, count=1000), Droids(droidType=HEAVY_BATTLE_DROID, count=600), Droids(droidType=DROIDEKA, count=200)], shipName=Cutter)  
  
***Task 2*** Writing to stream with/without buffer compare characteristic  
  
File size: 2Mb = 52428800 bytes  
  
Writing without buffer: 150141 ms  
Reading without buffer: 136718 ms  
  
Writing with buffer (default size): 2336 ms  
Reading with buffer (default size): 1713 ms  
  
Writing with buffer (buffer size: 1): 160657 ms  
Reading with buffer (buffer size: 1): 142930 ms  
  
Writing with buffer (buffer size: 4): 46279 ms  
Reading with buffer (buffer size: 4): 31527 ms  
  
Writing with buffer (buffer size: 16): 11692 ms  
Reading with buffer (buffer size: 16): 4449 ms  
  
Writing with buffer (buffer size: 32): 6524 ms  
Reading with buffer (buffer size: 32): 2651 ms  
  
Writing with buffer (buffer size: 64): 4219 ms  
Reading with buffer (buffer size: 64): 1175 ms  
  
Writing with buffer (buffer size: 256): 2633 ms  
Reading with buffer (buffer size: 256): 1970 ms  
  
Writing with buffer (buffer size: 1024): 1877 ms // Increasing buffer size does not positive impact on write/read effectivity  
Reading with buffer (buffer size: 1024): 1912 ms  
  
Writing with buffer (buffer size: 4096): 1690 ms  
Reading with buffer (buffer size: 4096): 1521 ms  
  
Writing with buffer (buffer size: 26214400): 1656 ms  
Reading with buffer (buffer size: 26214400): 1548 ms  
  
Writing with buffer (buffer size: 52428800): 1916 ms  
Reading with buffer (buffer size: 52428800): 1546 ms  
  
Writing with buffer (buffer size: 104857600): 1850 ms  
Reading with buffer (buffer size: 104857600): 1629 ms  
  
***Task 3*** Custom implementation of PushbackStream  
  
49  
50  
51  
52  
0  
0  
0  
53  
54  
55  
56  
57  
! Zeroes was pushed back to the stream and read from  
  
***Task 4*** Comment searcher (Used file: src/task4/CommentSearcher.java)  
  
NEW COMMENT:  
/**  
     * args[0] must contain file name with path to handle.  
     * @param args  
     */  
NEW COMMENT:  
/**  
     * Seeks and prints whole the comments of specified file in <code>args</code> parameter.  
     */  
NEW COMMENT:  
// THIS IS COMMMMMMMMMENT!!!  
NEW COMMENT:  
// And this  
NEW COMMENT:  
/*  
    I can change the world. Perhaps.  
     */  
  
***Task 5*** File tree  
  
Directory: classwork1 [readble, not hidden]  
Directory: consolegame [readble, not hidden]  
Directory: demo [readble, not hidden]  
Directory: diplom [readble, not hidden]  
Directory: dutka [readble, not hidden]  
Directory: greatprojectwithspring [readble, not hidden]  
Directory: gs-async-method [readble, not hidden]  
Directory: JUnit4 [readble, not hidden]  
Directory: JUnit5 [readble, not hidden]  
Directory: master [readble, not hidden]  
Directory: SpringFirstLook [readble, not hidden]  
Directory: task02_OOP [readble, not hidden]  
Directory: task03_Arrays_Collections [readble, not hidden]  
Directory: task04_collections2_bintree [readble, not hidden]  
Directory: task05_Log4j [readble, not hidden]  
Directory: task06_Lambdas_Streams [readble, not hidden]  
Directory: task07_String [readble, not hidden]  
Directory: task08_Annotation [readble, not hidden]  
Directory: task09_JUNIT [readble, not hidden]  
Directory: task10_IO_NIO [readble, not hidden]  
Directory: task11_MultiThreading_1 [readble, not hidden]  
Directory: task12_MultiThreading_2 [readble, not hidden]  
Directory: task13_XML [readble, not hidden]  
Directory: task14_JSON [readble, not hidden]  
Directory: task18_JDBC [readble, not hidden]  
Directory: task19_Hibernate [readble, not hidden]  
Directory: task20_SpringDataJPA [readble, not hidden]  
  
***Task 6*** Custom buffer  
  
123sdad  
0  
! As expected  
  
***Task 7*** Client-server chat (Several clients chats on one server)  
![picture](1.png)